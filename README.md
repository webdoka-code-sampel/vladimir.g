# vladimir.g


## The svg-Title module for Wordpress.

The module adds the ability to make svg labels in the administrative part. A shortcode is created in the module. You can choose the color, size, animation, and font rendering from google. The output is an svg code. This shortcode can be inserted on a website page or in a widget. When it appears, the text is animated.


- [Link to the repository](https://gitlab.com/webdoka-code-sampel/vladimir.g/-/tree/svg-title/svg-title?ref_type=heads)